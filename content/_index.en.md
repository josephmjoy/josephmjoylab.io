---
title: "JMJ Docs"
---

# Welcome

This site documents some of my projects and experiences. It also lists some useful resources.

{{% notice note %}}
On mobile devices, you may need to select a little icon on the top left to bring up the table of contents.
{{% /notice %}}

This site is built using Hugo, an open-source static site generator. The process I follow is documented [here]({{%relref "projects/building-jmjdocs" %}}).

