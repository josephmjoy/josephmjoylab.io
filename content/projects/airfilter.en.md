---
title: Air Filter
weight: 10
aliases:
    - /airfilterproject # Don't change this - this link is shared externally.
---


{{% notice note %}}
The Air Quality Index (AQI) is computed differently for different countries. In the US, it is a single number computed by considering the dominant contributor among various pollutants,
including aerosol particles of various dimensions ([Wikipedia](https://en.wikipedia.org/wiki/Air_quality_index#United_States)).
{{% /notice %}}

In September 2020, fires in Oregon and Washington states produced several days of unhealthy air quality in the Seattle area. I found (using a portable consumer-grade air quality monitor)
that running the fan in our home heating system for 2 hours twice daily was effective in improving air quality inside.

But, I thought, what if smoky conditions persist for days and replacement furnace air filters were hard to come by or were irrelevant (because, say, one lived in an apartment without central heating)?
Or what if things got so bad that it would be better to hunker down 
in a smaller room rather than "waste" the filter by filtering air in the whole house? There are various DIY air filter designs out there that (mostly) all connect a
fan to a replaceable furnace air filter. The design I came up with below provides about 20 sq feet of surface area over which something like a blanket can be draped - the blanket(s) can be washed periodically.
Preliminary measurements using my monitor indicate that this is effective. I used a synthetic blanket I happened to have lying around.
More experiments are needed trying various kinds of (readily available and reusable) filter materials. "Unfortunately" the air has cleared up now and so it will be hard to do realistic testing without finding some way to reproducibly generate smoky air. Meanwhile, I am happy to share this design for anyone to adopt, adapt and improve.

{{% notice note %}}
View and download images related to this article on [imgur](https://imgur.com/a/EqyScjC)
{{% /notice %}}

{{< figure
    src="https://i.imgur.com/ELfyYPP.jpg"
    title="Prototype air filter without filter cloth"
    caption="Walls made with welded steel fencing mesh. The cylinder diameter matches the fan, which rests on top."
    alt="cylinder made of steel mesh with a fan on top."
>}}


### Priliminary Measurements
The absolute readings from my meter are suspect (they tend to be about 2x the
values that are published on the Internet for our locality), but they are still useful because the readings definitely track air quality. For example, placing a dedicated HELPA air purifier
([Honeywell HPA300](https://www.honeywellstore.com/store/products/hpa300-true-hepa-whole-room-air-purifier-with-allergen-remover.htm)) in a closed room brings the particle
counts reported by the monitor close to zero, corresponding to reported AQI values under 10. I then took the monitor outside and it reported AQI values approaching 400 (yikes!).
It certainly was smoky outdoors, but The "official" (Internet reported) value for that time was around half that. Anyway, with this caveat, I found that running
the contraption my office for 20 minutes (with doors closed and central heating fan off) minutes with the synthetic blanket shown in the picture improved the AQI from above 50 to below 20.

