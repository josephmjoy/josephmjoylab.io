---
title: Circles Artwork
weight: 16
---
Our extended family made a framed artwork in 2020. It was a fun and collaborative project. Each of us had to contribute one 2.5" diameter circular artwork,
and these were assembled in a grid pattern on a rectangular backing that would be framed. We needed to do is figure out the spacing.
We considered various rectangular and hexagonal grids. We used the open source [Processing](http://processing.org) framework to visualize the options,
and also to print out a full scale template that we used to assemble the circular pieces of artwork contributed by each of our family members.
