---
title: Building JMJ Docs
weight: 1
---

This article documents the process I use to build and host this site, and the reasoning behind various decisions I made along the way. It is targeted at myself, so I can remember how and why I did something,
and also for others who are *not* familiar with static web sites and hosting them on Github or GitLab. I am incapable of content layout and styling. Furthermore, while I am familiar with the 
technology that underpins much of this, I am, in the end, a "systems person" who needs to look up how to do this or that with HTML, JavaScript, CSS and so forth. Fortunately, with this setup, I do not need to do any of that and can focus on generating content.

## Background {#background}

### Static Site Generators and Hosting Options {#ssg-hosting}

A *static web site* is a collection of files that the hosting web server just has to serve -- without any processing. Furthermore, these files, assuming they do not change too often, can be cached in CDN services and the client's browser cache, so the site can be quite performant in terms of network bandwidth and latency. It may seem at first glance that these sites will be completely static in terms of how they interact with a user - like a published magazine. But because these files can contain JavaScript that executes on the client's browser, and this JavaScript can among other things contact 3rd-party sites (such as comment hosting servers and map servers) one can create surprisingly interactive sites as static web sites.

A *static site generator* is software that generates and updates static web sites. They transform a representation of the sites' content that is easier for humans to edit, into raw HTML, JavaScript, CSS and other resources that are served to clients.

Some of the most popular static site generators (all Open Source) are:
 - [Jekyll](https://jekyllrb.com/) - one of the earliest and usually shows up in top lists as the most popular; uses Ruby.
 - [Hexo](https://hexo.io/) - uses Node.js.
 - [Gatsby](https://www.gatsbyjs.com/) - uses React.js.
 - [Hugo](https://gohugo.io/) - uses Go.

Some features shared by all the popular static site generators are:
 - Support for Markdown text, which is very popular with those, like me, who like to author content in plain text that is maintained in a version control system.
 You are presumably familiar with Markdown, but if not, read more about Markdown on [Wikipedia](https://en.wikipedia.org/wiki/Markdown) and in in this [tutorial](https://www.markdownguide.org/basic-syntax/)
 (one of many out there; I like this one because it recommends best practices given the many implementations of Markdown).
 - Support for themes, authored by people who know how to design web sites, and used by people like me who do not. Many of the themes provide "responsive design" that fluidly adapts to different display devices including mobile phones. All of the design elements of *this* site is thanks to the [Learn Hugo theme](https://themes.gohugo.io/hugo-theme-learn/), which is a port of the similarly-named theme in [Grav](https://getgrav.org/), yet another static site generator (actually it offers more than that).
 - Provide many features aimed at minimizing the amount of duplicated text and code that you have to edit to maintain the site. For example, simply adding a Markdown file with page content in a particular location in the folder structure will automatically regenerate navigation links, menus and indexes (the navigation panel to the left of this site is generated automatically).
 - Can rapidly rebuild and preview your site on your development machine. This is a great time saver when making frequent edits. In fact, simply saving a Markdown file automatically triggers a browser refresh.
 - Have a vibrant community of users who contribute themes, answer questions and author high quality tutorials.
 
Another great feature of some of these generators, Jekyll and Hugo in particular, is their tight integration with GitHub and GitLab. Once set up, simply pushing an updated markdown file will automatically
trigger a re-build and re-publishing of your site, hosted by GitHub and GitLab pages! These sites leverage their Continuous Integration (CI) process to run the build and I have found both services
very responsive - typically the site is updated within a minute of pushing the new content. GitHub natively supports Jekyll, and GitLab supports Hugo (among others).

### Why I picked Hugo, GitLab, and the Hugo Learn Theme {#why-hugo-gitlab-learn}

After considering various alternatives, I have settled on Hugo, hosted on GitLab. I like Hugo because it is just a single executable to run on my developer machine, is very fast at rebuilds, and has more
than enough variety of high quality themes to select. I picked GitLab because of its support for Hugo, and (even though I do not use it currently), has free support for private projects. I will use Jekyll
as needed for technical-documentation associated with software I write on Github.

Hugo offers a huge number of community-contributed themes. Check them out [here](https://themes.gohugo.io/). How does one pick a theme? It can be overwhelming. Picking a theme is an investment because
it is not trivial to change themes - inevitably one starts use custom features provided by a particular theme. To pick a theme, I started with a shortlist based on how many "stars" 
the theme had accumulated on GitHub, as reported on this unofficial site: https://pfht.netlify.app/post/top-starred/.
I looked at the demo pages for the top 30 or so themes (from both laptop and mobile phone to check for responsive design). I was looking for themes that were designed for documentation 
(as opposed to, say, blogs or company sites), and that had a minimalist feel to them. For those that seemed promising I then looked at the quality and amount of documentation and how actively the theme
was being worked on in Github. Some of the themes required the 'extended' version of Hugo, that had support for the [SASS](https://sass-lang.com/) DSL for CSS. I opted for those that picked the standard (default) version of Hugo. Based on all these informal criteria, the [Learn](https://themes.gohugo.io/hugo-theme-learn/) theme bubbled up to the top for me. I am happy with it.

Incidentally, the theme with the most number of Github stars (by far) is the [Academic](https://themes.gohugo.io/academic/) theme. It is fearsomely sophisticated and has its own plugin system. I may 
get back to it in a future but it really seems overkill given my somewhat modest requirements for this site.

### An Aside on SSH Authentication {#ssh-authentication}
Github and GitLab provide two secure communication protocols - HTTPS(password)-based and SSH(key)-based. The choice is encoded in the URL to the repository:
- HTTPS: `git@gitlab.com:some-username/some-projectname.git`
- SSH: `https://gitlab.com/some-username/some-projectname.git`

With HTTPS you use your GitLab/Github username and password, and with SSH you use public-private key pairs. The private key is kept securely on the client, and the public key has been previously
registered with the server. I am presently using SSH keys to manage this site on GitLab, primarily because it was something I was not previously doing
and it seems to be preferred as a general way to communicate securely with servers, especially Linux servers.

The main benefits of using SSH are:
- Resistance to brute force attacks.
- Allows one person to have individual keys for each client machine. If any one client is compromised, the key pair associated with that client can be purged from all servers (in theory, but see below,
  under 'burdens').
- One can use a shorter passphrase to encrypt the private key on the client, assuming the client is less exposed to brute force attacks in general. Furthermore, access to a private key can be
  maintained in-memory by an SSH agent on the client, so that scripts that need to do remote logins will not need to embed sensitive passwords or bring up UI for the user to enter passwords.

One of the main burdens of using SSH is key management. If not already familiar with this topic, it is worth investigating this further and internalizing best practices. See, for example, [this](https://www.beyondtrust.com/blog/entry/ssh-key-management-overview-6-best-practices) blog entry by enterprise security provider BeyondTrust. See also [this](https://www.redhat.com/sysadmin/passwordless-ssh) article from RedHat.

Some best practices:
- Do not share keys among multiple individuals.
- Ideally, do not share keys among multiple client machines. Copying the private key files among client machines is discouraged as it precludes one of the key benefits of using SSH, namely being
  able to revoke privileges from a specific client machine.
- Use a passphrase for protecting your private keys. It seems that though this is the recommendation, people often use no passphrases, assuming hackers will never get to their private keys.
  You can change/add/remove a passphrase for a specific key using `$ ssh-keygen -p`. 
- Use `$ ssh-keygen -t ed25519 -a 100` to generate particularly secure key pairs. This uses the ED25519 key generation algorithm,
  which uses the [EdDSA](https://en.wikipedia.org/wiki/EdDSA) algorithm with specific parameters, including SHA-512.
  For even more protection against a lack of randomness, `-a 100` option adds additional rounds in the generation process. To be clear,
  this is all overkill for administering *this* site; I am simply echoing recommendations from various articles and tutorial videos.
- Add a suffix to the key files that identifies your client, so it is easier to manage.
- Use `ssh-agent` to manage access without re-entering the passphrase for each transaction.


## Getting Started {#getting-started}

### Summary {#getting-started-summary}

I followed steps in https://gohugo.io/getting-started/quick-start/, https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/,  and also watched a few YouTube videos to get a broader context.
The high level steps below are just a taste for what is ahead before you dive into the documentation and videos:
- *Make an empty GitLab project for your site.* At this stage it is just a folder somewhere on your desktop machine.
  If, like this site, the site is ultimately to be published on GitLab pages as `https://your-gitlab-username.gitlab.io`, i.e., a *user* page,
  this project must be named specifically `your-gitlab-username.gitlab.io`. Else it can be any valid project name,
  and will be published as `https://your-gitlab-username.gitlab.io/some-project-name`. You can have just one user site and any number of project sites.
- *Add the Learn theme as git submodule `learn` under `./themes`*.
- *Create some content.* You can now either populate the site manually following the Hugo and Learn instructions,
  or you can (as I did) copy all the files and folders under the Learn theme example site `./themes/learn/exampleSite`
  to the top level of your project. NOTE: One change is to remove the `themesDir` line in `config.toml` configuration file present the the example site.
- *Build the site locally.* Since there are no draft pages in this example
  site, you can just run the `hugo` command without any parameters to build. The build process (very fast) puts all the generated content under the `./public` folder.
  This folder should be excluded from checkin, so add it to the `.gitignore` file for your project.
- *Preview your site.* You can now preview your contents by running `hugo server` in a separate console window,
   and navigate to the URL printed to the console, which will be something like `http://localhost:1313/`
- *Push to GitLab.* Set the git origin of our project to your GitLab server account and push it to the GitLab server. Perhaps you had set the origin already by starting the project on GitLab and
   cloning it locally.
- *Publish to GitLab pages.* Thus far the project has been pushed, but GitLab's CI process has not yet been engaged to automatically build your site and push it to GitLab pages.
   This requires one final step: create a `.gitlab-ci.yml` file at the project level based on the instructions in https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/.
- *Prepare to add your own content.* If you got started using the Learn example site mentioned above, you now have some cleaning up to do,
   because the example site is quite a few pages, and multilingual support. I waited until I had verified 
  that the example site was published in Gitlab pages and looked good before I gutted the site and begin to add actual content. I did this because I wanted to verify for myself that the more complex
  content and navigation rendered correctly. This is something that only makes sense to do once for your very first site, and mainly as a learning experience.

### Installing and customizing the Hugo Learn Theme {#learn-theme}

The Learn theme is added as a Git [Submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). 
These are the customizations I made after installing it:
- Removed additional items added to the bottom of the menu, by removing  the `[[Languages.en.menu.shortcuts]]` section in `config.toml`. It used to something like:
    ```
    no "MORE" section.
    [[Languages.en.menu.shortcuts]]
    name = "<i class='fab fa-fw fa-github'></i> GitHub repo"
    identifier = "ds"
    url = "https://gitlab.com/josephmjoy/josephmjoy.gitlab.io"
    weight = 10
    ```
- Replaced content of the menu footer, that lives in `layouts/partials/menu-footer.html`.  It turns out that deleting the
   file simply makes Hugo pick up the default one from the Learn theme, saved under `themes/learn/layouts/partials`.
- Deleted the Github contributor stats, which is the file  `layouts/shortcodes/ghcontributors.html`
- Replaced the logo - this is the file `layouts/partials/logo.html` by replacing the SVG content in the file with content created by Inskcape. More details in the [SVG Content](#svg-content) section.


## Incorporating Rich Media {#rich-media}

### Creating SVG content using Inkscape {#svg-content}
Where possible, images and diagrams are represented as SVG. The key benefits being small size and being text-based,  maintaining a fine-grained version history. [Inskcape](https://inkscape.org/),
the popular Open Source vector drawing program, can be used to create and edit SVG content. For example, the "JMJ Docs" site logo was created using this process. 
This file is saved as `PROJECTDIR/inkscape/logo.svg`. I plan to place other
Inkscape-generated figures there. Initially I had made an image with too small dimensions, to it was barely visible on the site. The current dimensions are 1058 by 200 pixels. These can be
set from the `File -> Document Properties` dialog on Inkscape. I also initially tried simply specifying SVG text, set with "Bauhaus 93" font. This appeared to render fine on my desktop, but it
rendered a generic font on my Android phone. So I converted the text to path objects (select the text, then run `Path -> Object to Path`. The resultant SVG content renders great and makes
up the current "JMJ Docs" logo. Note that Inkscape's native format is a kind of enhanced SVG with Inkscape-specific elements - these are supposed to be ignored when rendered by non-Inkscape entities
like browsers. However, since we are folding in the contents into an HTML file, just copy the path information into the existing SVG element in the logo file, replacing the paths that were there for
the Learn logo.

### Serving Images and Video {#images-and-video}

Hugo supports several ways to include images and other resources located *within* the site itself:
1. Create a folder structure under `PROJECTDIR/static`, (for example, `PROJECTDIR/static/project/article/foo.png`)  and save the resource files there. The files may then be referenced within
   markdown (leave out the `PROJECTDIR/static` portion of the path, for example: `project/article/foo.png`).
2. Side-by-side with Markdown content, using a Hugo concept known as [Page Bundles](https://gohugo.io/content-management/page-bundles/). This keeps various resources in the same folder as an
   article itself. There seems to be a sophisticated image processing pipleine that allows generation of multi-resolution image sets to be served selectively depending on the client's capabilities.
3. Text-based DLSs (SVG can be considered a part of this too), that can be embedded within HTML partials, or even systems such as that support embedding diagrams *within* Markdown
   using shortcodes. A notable example of the latter is [Mermaid](https://learn.netlify.app/en/shortcodes/mermaid/), which is supported "natively" by the Learn theme. More on Mermaid later.

Of course, markdown supports referencing images a other media from anywhere on the internet, so that is another option to consider. Images and video can be quite large, and in fact
could completely dominate textual content on the site. I decided all binary image and video on this (GitLab-hosted) site would be hosted elsewhere, on sites that are designed to serve such media. 

Requirements:
- No ads.
- Free or low cost.
- Easy to upload images, maintain lots of small "galleries", one per article on the site.
- Well known, respected site.
- Optional: automatically creates multiple-resoution versions of images.

Here are some consulted that list "top-*N*" sites for hosting images on the Internet:
- https://www.lifewire.com/free-image-hosting-sites-3486329
- https://www.techradar.com/best/best-image-hosting-websites
- https://filmora.wondershare.com/photo-editing/best-image-hosting-sites.html


Based on these recommendations, I have classified some of the better known ones:
 - [postimage](https://postimages.org/), [imgbox](https://imgbox.com/), [imgbb](https://imgbb.com/): Theese sites are notable in that they provide unlimited, ad-free hosting of images, "for ever".
    Their revenue model is unclear. They allow hosting of images indifinetly even without creating an account.
    I decided not to rely on these services for this site, because their viability for the long term is unclear and I am slightly concerned about what they do with these images to generate revenue.
 - [Flikr](https://www.flickr.com/) and other sites like it: Cater to sharing photographs and albums among photo enthusiasts, both amature and professional.
 - [DropBox](https://www.dropbox.com/), [Google Drive](https://www.google.com/drive/), Microsoft's [OneDrive](https://www.microsoft.com/en-us/microsoft-365/onedrive/online-cloud-storage)
 - [Google Photos](https://photos.google.com/)
Video hosting options:

- Vimeo, YouTube

### Enable Math Typesetting with KateX
Katex is a JavaScript library to render math typesetting expressed as LaTeX.
More info here: https://eankeen.github.io/blog/posts/render-latex-with-katex-in-hugo-blog/


### Enable Mermaid, a DSL for Diagrams
Mermaid is integrated into the Learn theme. Enable it just by setting the property `disableMermaid` to `false`, which can be done on a per page basis (TODO: check)


### Include Github Gists
Built in Hugo shorcode `gist` embeds a Github [gist](https://docs.github.com/en/free-pro-team@latest/github/writing-on-github/creating-gists) in the site.

## Cookbook {#cookbook}

### Markdown and Hugo Shortcode Tips {#markdown-shortcode-tips}
- Add a custom anchor to a heading like this: `# An Aside on SSH Authentication {#ssh-authentication}`. Documentation [here](https://gohugo.io/content-management/cross-references/#heading-ids).
- Disable TOC
- Disable inline copy-icon for inline code
- Hide `<` and `>` navigation aids for next/previous article
- Mark a page as draft
- Hugo standard shortcodes and Learn-specific shortcodes
- `%...%` vs `<...>` inside shortcodes.
- Slugs, permalinks, aliases
- List of shortcodes actually used on the site, with links to a section where it is used

