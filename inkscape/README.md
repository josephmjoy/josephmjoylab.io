Inkscape files for generating SVG content live here. For now I manually cut and paste content into the desired location:
- logo.svg: cut the `<path>` and `<text>` elements into `partials/logo.html`.
